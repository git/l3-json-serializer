<?php

namespace elanpl\L3\JSONSerializer;

class JSONSerializer implements \elanpl\L3\ISerializer{
    public function serialize($viewModel)
    {
        return \json_encode($viewModel->getDTO());
    }
}